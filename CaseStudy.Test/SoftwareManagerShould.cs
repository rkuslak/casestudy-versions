namespace CaseStudy.Tests
{
    using CaseStudy.Managers;
    using System.Linq;
    using Xunit;

    public class SoftwareManagerShould
    {
        [Theory]
        [InlineData("0.0.1", new int[] { 0, 0, 1 })]
        [InlineData("0.1.2", new int[] { 0, 1, 2 })]
        [InlineData("0.1", new int[] { 0, 1 })]
        [InlineData("1", new int[] { 1 })]
        public void ParseVersionElementTest(string versionString, int[] expectedResultArray)
        {
            var expectedResult = expectedResultArray.ToList();
            var results = SoftwareManager.TryParseVersionFromString(versionString);

            var resultsLength = results.Count();

            Assert.Equal(resultsLength, expectedResult.Count());

            var result = results.GetEnumerator();
            var expect = expectedResult.GetEnumerator();
            for (var x = 0; x < resultsLength; x++)
            {
                result.MoveNext();
                expect.MoveNext();
                Assert.Equal(result.Current, expect.Current);
            }
        }

        [Theory]
        [InlineData("0.0.1", 9)]
        [InlineData("0.1.2", 8)]
        [InlineData("0.1", 8)]
        [InlineData("2017", 2)]
        [InlineData("2019", 1)]
        [InlineData("2020", 0)]
        public void CompareVersionNumbersTest(string comparer, int expectedNumberOfResults)
        {
            var results = SoftwareManager.GetSoftwareAfterVersion(comparer);
            Assert.Equal(results.Count(), expectedNumberOfResults);
        }
    }
}
