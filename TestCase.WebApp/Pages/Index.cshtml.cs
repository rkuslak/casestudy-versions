﻿using CaseStudy.Interfaces;
using CaseStudy.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;

namespace TestCase.WebApp.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public String SearchString { get; set; }
        public String SearchError { get; set; }
        public IEnumerable<Software> MatchedSoftware
        {
            get;
            set;
        } = SoftwareManager.GetAllSoftware();

        public void OnGet()
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                try
                {
                    var results = SoftwareManager.GetSoftwareAfterVersion(SearchString);
                    MatchedSoftware = results;
                }
                catch
                {
                    // Set ViewModel bad
                    // TODO: Show on Index.cshtml
                    SearchError = "Invalid search string";
                    MatchedSoftware = SoftwareManager.GetAllSoftware();
                }
            }
        }
    }
}
