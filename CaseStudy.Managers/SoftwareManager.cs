﻿
namespace CaseStudy.Managers
{
    using CaseStudy.Interfaces;
    using System;
    using System.Collections.Generic;

    public static class SoftwareManager
    {
        public static IEnumerable<Software> GetAllSoftware()
        {
            return new List<Software>
            {
                new Software
                {
                    Name = "MS Word",
                    Version = "13.2.1."
                },
                new Software
                {
                    Name = "AngularJS",
                    Version = "1.7.1"
                },
                new Software
                {
                    Name = "Angular",
                    Version = "8.1.13"
                },
                new Software
                {
                    Name = "React",
                    Version = "0.0.5"
                },
                new Software
                {
                    Name = "Vue.js",
                    Version = "2.6"
                },
                new Software
                {
                    Name = "Visual Studio",
                    Version = "2017.0.1"
                },
                new Software
                {
                    Name = "Visual Studio",
                    Version = "2019.1"
                },
                new Software
                {
                    Name = "Visual Studio Code",
                    Version = "1.35"
                },
                new Software
                {
                    Name = "Blazor",
                    Version = "0.7"
                }
            };
        }

        /// <summary>
        /// Takes a passed version string and attempts to split it into
        /// "elements", delimited by a period in the initial string.
        /// </summary>
        /// <param name="versionString">String representing the version to pull
        /// elements from</param>
        /// <returns>List of integers for the split version string</returns>
        public static IEnumerable<int> TryParseVersionFromString(string versionString)
        {
            var results = new List<int>();
            var stringElements = versionString.Trim().Split('.');

            foreach (var elementString in stringElements)
            {
                int element = 0;

                if (!string.IsNullOrWhiteSpace(elementString))
                {
                    if (!Int32.TryParse(elementString, out element))
                    {
                        // TODO: Verify integer-only versions are expected (no
                        // "12z.10.1") AKA we're only concerned with semver
                        throw new Exception($"Invalid version string passed: {versionString}");
                    }
                }
                results.Add(element);
            }
            return results;
        }

        /// <summary>
        /// Tests if two version element Lists are equal or of the
        /// versionElement set is large, returning true if so or false otherwise
        /// </summary>
        /// <param name="comparerElements">Elements to compare against the existsing version</param>
        /// <param name="versionElements">Elements of version to test against</param>
        /// <returns>False if versionElements is lower than comparerElements, true otherwise</returns>
        public static bool CompareVersionsAreEqualOrLarger(IEnumerable<int> comparerElements, IEnumerable<int> versionElements)
        {
            // Compare each direct level of direct elements, returning true if we know the compared version is large:
            var comparerEnumerator = comparerElements.GetEnumerator();
            var versionEnumerator = versionElements.GetEnumerator();
            while (comparerEnumerator.MoveNext())
            {
                int version = 0;
                if (versionEnumerator.MoveNext())
                {
                    version = versionEnumerator.Current;
                }

                if (comparerEnumerator.Current < version)
                {
                    return true;
                }

                if (comparerEnumerator.Current > version)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns a List of software versions from the central store which are
        /// equal to or after the passed version string.
        /// </summary>
        /// <param name="versionString">Version string to compare stored
        /// versions with</param>
        /// <returns>List of software with a version equal to or large than the
        /// passed version string</returns>
        public static IEnumerable<Software> GetSoftwareAfterVersion(string versionString)
        {
            var results = new List<Software>();
            var comparerElements = TryParseVersionFromString(versionString);

            foreach (var software in GetAllSoftware())
            {
                var versionElements = TryParseVersionFromString(software.Version);
                if (CompareVersionsAreEqualOrLarger(comparerElements, versionElements))
                {
                    results.Add(software);
                }
            }

            return results;
        }
    }
}
